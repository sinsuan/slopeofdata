from function import *

#匯入 data
raw_data = pd.read_csv('電鍍配方2019-07-01_16-12-32.csv')
X = raw_data[['電鍍表面積(dm²)', '凹槽加權(%)' , '電鍍掛勾產品數量' , '掛勾數量']]
y = raw_data[['鉻槽電壓V1' , '鉻槽電壓V2' , '鎳槽電壓V1' , '鎳槽電壓V2']]

R = [] #因為要找函數，阿函數就是 relation ，所以代號用 R
for ind in range(120): #資料:[編號 , y , X] ，因為 y 只有一維，所以放前面比較整齊
    R.append([ind , y['鉻槽電壓V1'][ind] , y['鉻槽電壓V2'][ind] , y['鎳槽電壓V1'][ind] , y['鎳槽電壓V2'][ind] , X['電鍍表面積(dm²)'][ind] , X['凹槽加權(%)'][ind] , X['電鍍掛勾產品數量'][ind] , X['掛勾數量'][ind]])

#計算「邊長」
com_dif = []
for i in range(len(R) - 1):
    for j in range(i + 1 , len(R)):
        com_dif.append(compare_difference(R , i , j , 5 , 8 , 1 , 5))

#計算「斜率」
temp = []
for i in range(len(com_dif)):
    temp.append(ratio(com_dif[i] , 1 , 2))
ratio_com_dif = []
same_data = [] #裡面放數值一樣的 pair 的編號
for i in range(len(temp)):
    if isinstance(temp[i][1] , str):   
        same_data.append(temp[i][0])
    else:
        ratio_com_dif.append(temp[i])

#重複的數據
for i in range(len(ratio_com_dif)):
    print(same_data[i])

#列出排列過比值
L = indexSort(ratio_com_dif , 1)
K = []
for i in range(len(ratio_com_dif)):
    K.append([i , L[i]])
for i in range(len(ratio_com_dif)):
    print(K[i])

#挑出 dist_X 跟 dist_y
A = [] #dist_X
B = [] #dist_y
for ind in range(len(com_dif)):
    A.append(com_dif[ind][2])
    B.append(com_dif[ind][1])

#繪製 dist_X 跟 dist_Y 的圖
change_range_linearly(A , 0 , 1 , 0)
change_range_linearly(B , 0 , 1 , 0)    
plt.figure()
plt.plot(A , B , '.')
plt.grid()
plt.show()