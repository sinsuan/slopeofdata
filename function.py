import pandas as pd
import numpy as np
from matplotlib import pyplot as plt #繪圖
np.set_printoptions(suppress=True) #不要用科學計號，以便閱讀

"""
當資料量太大時沒辦法用 recursive (which quickSort uses) ，所以寫了這個
對於每一個元素a，將a與其他人比大小，若有人，稱b，比a大，則a的積分減一、b的積分加一，反之。
計算完後，會得到一組list，裡面的數字落在閉區間[-len(sorted),-len(sorted)]當中
最後將此list所有數字加上len(sorted)後再除以二，則得到真正的排序
"""
def indexSort(sorted , n = 0): 
    if len(sorted) <= 1:
        print("It is empty.")
    else:
        indices = [0] * len(sorted) #執行完成後 indices[i] 代表 sorted[i] 的排序
        result = [0] * len(sorted)  #執行完成後，便是由小排到大的 sorted
        if isinstance(sorted[0] , list):
            for i in range(len(sorted)):
                for j in range(i + 1 , len(sorted)):
                    if sorted[i][n] > sorted[j][n]:
                        indices[i] = indices[i] + 1
                        indices[j] = indices[j] - 1
                    else:
                        indices[i] = indices[i] - 1
                        indices[j] = indices[j] + 1
        else:
            for i in range(len(sorted)):
                for j in range(i + 1 , len(sorted)):
                    if sorted[i] > sorted[j]:
                        indices[i] = indices[i] + 1
                        indices[j] = indices[j] - 1
                    else:
                        indices[i] = indices[i] - 1
                        indices[j] = indices[j] + 1
        for ind in range(len(indices)):
            indices[ind] = int((indices[ind] + len(indices) - 1) / 2) #要減一，因為 n 個東西排序，每個東西只會跟 n-1 的東西比
            result[indices[ind]] = sorted[ind]
        return result


#計算兩點間的長度
def distance(A , term1 , term2 , begdim , enddim):
    temp = 0 #計算 X 長度
    for ind in range(begdim , enddim):
        temp = temp + (A[term1][ind] - A[term2][ind])**2
    return np.sqrt(temp)

#計算兩組數據的「邊長」
def compare_difference(A , term1 , term2 , beg_X , end_X , beg_Y , end_Y): 
    return [(term1 , term2) , distance(A , term1 , term2 , beg_Y , end_Y) , distance(A , term1 , term2 , beg_X , end_X)]

#計算「斜率」
"""
取對數是為了不被倒數影響，方便人判讀。
"""
def ratio(A , mole , deno):
    r = np.log10(A[mole] / A[deno])
    if np.isnan(r):
        return [A[0] , "0/0"]
    else:
        return [A[0] , r]

#改變數據範圍
def change_range_linearly(A , low , high , n = 0): #把 A[n] (如果不是矩陣，只是 list ，就 A) 的數值線性縮放到閉區間 [low , high]
    if len(A) <= 1:
        print("It is empty.")
    else:
        if isinstance(A[0] , list):
            temp = indexSort(A , n)
            L = temp[0][n]
            H = temp[len(A) - 1][n]
            for i in range(len(A)):
                A[i][n] = (A[i][n] - L)*(high - low) / (H - L) + low
        else:
            temp = indexSort(A)
            L = temp[0]
            H = temp[len(A) - 1]
            for i in range(len(A)):
                A[i] = (A[i] - L)*(high - low) / (H - L) + low